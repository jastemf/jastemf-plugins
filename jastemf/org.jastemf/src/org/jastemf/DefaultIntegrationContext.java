/**
 * <copyright>
 *
 * This program and the accompanying materials are made available under the
 * terms of the BSD 3-clause license which accompanies this distribution.
 *
 * </copyright>
 */
package org.jastemf;

import java.util.Set;

import org.eclipse.core.resources.*;
import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

/**
 * Abstract integration context, that supports default implementations for all
 * access methods derivable from the integration's {@link
 * IIntegrationContext#genmodel() generator model}, {@link
 * IIntegrationContext#astpackage() AST class package} and {@link
 * IIntegrationContext#outpackage() integration artifacts package}.
 * @author C. Bürger
 * @author Sven Karol
 */
public class DefaultIntegrationContext implements IIntegrationContext {
	
	private IProject project = null;
	private IJavaProject jProject = null;
	
	private GenModel genModel = null;
	private String outPackage = null;
	private String astPackage = null;
	private String jaddcmd = null;
	private Set<String> jragspecs = null;
	private boolean genEditCode = false;
	private boolean programmaticRefactorings = true;	
	
	public DefaultIntegrationContext(GenModel genModel, String outPackage, String astPackage, String jaddcmd, Set<String> jragspecs, boolean genEditCode, boolean programmaticRefactorings) {
		this.genModel = genModel;
		this.outPackage = outPackage;
		this.astPackage = astPackage;
		this.jaddcmd = jaddcmd;
		this.jragspecs = jragspecs;
		this.genEditCode = genEditCode;
		this.programmaticRefactorings = programmaticRefactorings;
	}
	
	public IProject workspaceProject(){
		if(project==null)
			project = ResourcesPlugin.getWorkspace().getRoot().getProject(genmodel().getModelDirectory().split("/")[1]);
		return project;
	}
	
	public IJavaProject javaProject(){
		if(jProject==null)
			jProject = JavaCore.create(workspaceProject());
		return jProject;
	}
	
	public GenModel genmodel() {
		return genModel;
	}

	public String outpackage() {
		return outPackage;
	}

	public String astpackage() {
		return astPackage;
	}

	public String jaddcmd() {
		return jaddcmd;
	}

	public Set<String> jragspecs() {
		return jragspecs;
	}

	public boolean generateEditCode() {
		return genEditCode;
	}

	public boolean useProgrammaticRefactorings() {
		return programmaticRefactorings;
	};
}
