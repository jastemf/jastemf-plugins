/**
 * <copyright>
 *
 * This program and the accompanying materials are made available under the
 * terms of the BSD 3-clause license which accompanies this distribution.
 *
 * </copyright>
 */
package org.jastemf;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.jastemf.refactorings.APIAdaptationGenerator;
import org.jastemf.refactorings.AnnotationsGenerator;
import org.jastemf.refactorings.ExceptionsGenerator;
import org.jastemf.refactorings.ProgrammaticRefactorings;
import org.jastemf.refactorings.RefactoringManager;

import jastadd.JastAdd;


/**
 * <i>JastEMF's</i> main class to {@link
 * #performIntegration(IIntegrationContext) integrate} a
 * <i>JastAdd</i> evaluator and an <i>EMF</i> metamodel implementation.
 * @author C. Bürger
 * @author Sven Karol
 */
final public class IntegrationManager {
	
	private static String ADAPTATIONS_NAME = "RepositoryAdaptations";
	private static String EXCEPTIONS_NAME = "Exceptions";
	private static String ANNOTATIONS_NAME = "JastEMFAnnotations";
	
	/**
	 * Perform the integration of a project represented by an {@link
	 * IIntegrationContext integration context}.
	 * @param context Information about the project to integrate, which are
	 * required by the integration process.
	 */
	public synchronized static void performIntegration(
			IIntegrationContext context)
	throws JastEMFException {
		refreshIntegrationArtifacts(context);
		
		java.nio.file.Path adaptationsPath = Paths.get(new File(
				context.packagefolder(
						context.outpackage())).getAbsolutePath() +
			File.separator + ADAPTATIONS_NAME +".jrag");
		
		java.nio.file.Path exceptionsPath = Paths.get(new File(
				context.packagefolder(
						context.astpackage())).getAbsolutePath() +
			File.separator + EXCEPTIONS_NAME + ".java");
		
		java.nio.file.Path annotationsPath = Paths.get(new File(
				context.packagefolder(
						context.astpackage())).getAbsolutePath() +
			File.separator + ANNOTATIONS_NAME + ".java");
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");		
		String timeStamp = formatter.format(new Date());
		
		String adaptationsContents = new APIAdaptationGenerator(ADAPTATIONS_NAME, context, timeStamp).toString();
		String exceptionsContents = new ExceptionsGenerator(EXCEPTIONS_NAME, context, timeStamp).toString();
		String annotationsContents = new AnnotationsGenerator(ANNOTATIONS_NAME, context, timeStamp).toString();
		
		try {
			Files.createDirectories(adaptationsPath.getParent());
			Files.writeString(adaptationsPath, adaptationsContents);
			Files.createDirectories(exceptionsPath.getParent());
			Files.writeString(exceptionsPath, exceptionsContents);
			Files.createDirectories(adaptationsPath.getParent());
			Files.writeString(annotationsPath, annotationsContents);
		} 
		catch (IOException e) {
			throw new JastEMFException(e);
		}
		
		if(!context.useProgrammaticRefactorings()){
			throw new UnsupportedOperationException("Refactoring script is no longer supported!");
		}
		
		// Generate JastAdd evaluator
		int argCount = context.useProgrammaticRefactorings()?4:5; 
		String[] args = new String[argCount + context.jragspecs().size()];
		args[0] = "--rewrite";
		args[1] = "--package=" + context.astpackage();
		args[2] = "--o=" + new File(context.srcfolder()).getAbsolutePath();
		int i = 3;
		for (String spec:context.jragspecs())
			args[i++] = spec;
		args[i++] = new File(
				context.packagefolder(
						context.outpackage())).getAbsolutePath() +
			File.separator + ADAPTATIONS_NAME + ".jrag";			
		
		JastAdd.main(args);
		refreshIntegrationArtifacts(context);
		
		ProgrammaticRefactorings.renameASTClasses(context);
		
		RefactoringManager.performASTNodeAdaptations(context);
		RefactoringManager.performASTNodeStateAdaptations(context);
		RefactoringManager.performASTListAdaptations(context);
		RefactoringManager.performOptAdaptations(context);
		RefactoringManager.performASTClassesAdaptations(context);
		
		// Generate EMF model implementation
		IOSupport.generateGenModelCode(context.genmodel(), context.generateEditCode());
		RefactoringManager.beautifyGenPackages(context);
		
		refreshIntegrationArtifacts(context);
	}
	
	private static void refreshIntegrationArtifacts(
			IIntegrationContext context) throws JastEMFException {
		try {
			ResourcesPlugin.getWorkspace().getRoot().getFolder(
					new Path(context.genmodel().getModelDirectory())).
					refreshLocal(
							IResource.DEPTH_INFINITE,
							new NullProgressMonitor());
		} catch (CoreException e) {
			throw new JastEMFException(e);
		}
	}
}
